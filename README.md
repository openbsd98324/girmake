# girmake 

## The Portable Make for Makefile 

BSD, GNU, ... or GIRMAKE

````
clang girmake.c  -o girmake  

tcc girmake.c  -o girmake  
````


## Less vs more lines of code?

![](gir.png)




## Usage

girmake myprogram

This will read the "Makefile" and run commands for "myprogram:". 
The commands start with the char #9 ("tab").








